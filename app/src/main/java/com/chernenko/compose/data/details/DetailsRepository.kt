package com.chernenko.compose.data.details

import com.chernenko.compose.data.AppDatabase
import com.chernenko.compose.data.details.api.DetailsApi
import com.chernenko.compose.data.details.api.asDatabaseModel
import com.chernenko.compose.data.details.local.asDomainModel
import com.chernenko.compose.domain.Details
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

class DetailsRepository @Inject constructor(
    private val detailsApi: DetailsApi,
    private val appDatabase: AppDatabase
) {

    fun getUserDetails(user: String): Flow<Details?> =
        appDatabase.usersDao.getDetails(user).map { it?.asDomainModel() }

    suspend fun refreshDetails(user: String) {
        try {
            val userDetails = detailsApi.getDetails(user)
            appDatabase.usersDao.insertDetails(userDetails.asDatabaseModel())
        } catch (e: Exception) {
            Timber.w(e)
        }
    }

}