package com.chernenko.compose.data

import androidx.room.Database
import androidx.room.RoomDatabase
import com.chernenko.compose.data.details.local.DetailsEntity
import com.chernenko.compose.data.users.local.UserEntity
import com.chernenko.compose.data.users.local.UsersDao

@Database(entities = [UserEntity::class, DetailsEntity::class], version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract val usersDao: UsersDao
}