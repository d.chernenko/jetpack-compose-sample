package com.chernenko.compose.data.users

import com.chernenko.compose.data.AppDatabase
import com.chernenko.compose.data.users.api.UsersApi
import com.chernenko.compose.data.users.api.asDatabaseModel
import com.chernenko.compose.data.users.local.asDomainModel
import com.chernenko.compose.domain.User
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import timber.log.Timber
import javax.inject.Inject

class UsersRepository @Inject constructor(
    private val usersApi: UsersApi,
    private val appDatabase: AppDatabase
) {

    val users: Flow<List<User>?> =
        appDatabase.usersDao.getUsers().map { it?.asDomainModel() }

    suspend fun refreshUsers() {
        try {
            val users = usersApi.getUsers()
            appDatabase.usersDao.insertUsers(users.asDatabaseModel())
        } catch (e: Exception) {
            Timber.w(e)
        }
    }
}