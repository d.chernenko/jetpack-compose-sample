package com.chernenko.compose.domain

data class User(
    val id: Int,
    val avatar: String,
    val username: String
)