package com.chernenko.compose.ui.users

import com.chernenko.compose.domain.User

data class UsersUiState(
    val list: List<User> = listOf(),
    val offline: Boolean = false
)