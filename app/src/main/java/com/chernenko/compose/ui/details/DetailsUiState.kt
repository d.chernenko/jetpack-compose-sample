package com.chernenko.compose.ui.details

import com.chernenko.compose.domain.Details
import com.chernenko.compose.util.formatDate

data class DetailsUiState(
    val detail: Details = Details(),
    val offline: Boolean = false
) {
    val formattedUserSince = formatDate(detail.userSince)
}